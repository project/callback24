<?php

namespace Drupal\callbacktwentyfour\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class CallbacktwentyfourForm extends ConfigFormBase {
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'callbacktwentyfour_form';
  }
   /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
	  $host = \Drupal::request()->getHost();
	$user = \Drupal::currentUser()->getEmail();

    // Form constructor.
    $form = parent::buildForm($form, $form_state);
    // Default settings.
    $config = $this->config('callbacktwentyfour.settings');
	// Page title field.
    $form['page_title'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t("<iframe src='https://panel.callback24.io/users/pluginLogin/$user/$host/DRUPAL' style=' max-height:510px; max-width:500px; width: 500px; height: 510px; border: none; background: white; border-radius: 10px'></iframe>"),
      '#default_value' => $config->get('callbacktwentyfour.page_title'),
    ];
  
    return $form;
  }
  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('callbacktwentyfour.settings');
    $config->set('callbacktwentyfour.page_title', $form_state->getValue('page_title'));
    $config->save();
    return parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'callbacktwentyfour.settings',
    ];
  }

}