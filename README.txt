-- SUMMARY --

* Callback24 enables your website visitors to connect with you while browsing your site.
  By entering their phone number into pop up widget they can easily request
  callback instantly or schedule the call later. 

For more information about Callback24 visit our page https://callback24.ca or https://callback24.pl
  http://drupal.org/project/admin_menu

-- INSTALLATION --

* Install as usual, see http://drupal.org/node/1897420 for further information.

-- CONFIGURATION --

* After install you need to confugurate Callback24. Go to Extend» Callback24» Configure

  - Login to your Callback24 account or create new.(After registration you need to refresh
    configuration page on you website). That's all. After instalation you can personalise 
    Callback24 widget, and change some settings in Callback24 panel https://panel.callback24.io

-- CONTACT --

* development@callback24.io